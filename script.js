let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function(){
		console.log("Pikachu! I choose you!")
	}
};

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
	this.tackle = function(target){
		let targetRemainingHealth = target.health - this.attack
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + "'s health is now reduced to " + targetRemainingHealth);
		if(targetRemainingHealth <= 0){
			console.log(target.name + " fainted.");
		}
	}
	
}

console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);

console.log("Result of talk function:");
trainer.talk();


let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);
let rayquaza = new Pokemon("Rayquaza", 70);
let mewtwo = new Pokemon("Mewtwo", 100);

console.log(pikachu);
console.log(rattata);
pikachu.tackle(rattata);

console.log(rayquaza);
rayquaza.tackle(pikachu);
rayquaza.tackle(rattata);

console.log(mewtwo);
mewtwo.tackle(pikachu);
mewtwo.tackle(rayquaza);
